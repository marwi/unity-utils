scripts I wrote/collected/modified/used in previous projects and might be useful to someone else, too :monkey: :heart:


## Content
  
- [Json Scripts](#json-reading)
- [Simple HTTP Server](#simple-http-server)
- [Screenshots](#screenshotter)
- [Logging To File](#logging)

----

### Json Reading
save json for instant update in unity  
useful for e.g. persistent config parameters  
example included

![](http://uploads.gaisterhand.de/unityjson.gif)

---
### Simple Http Server
+ component for parameter parsing to dictionary  
useful for e.g. having a web application communicate with unity  
example included  

![](http://uploads.gaisterhand.de/unityhttp.gif)

---
### Screenshotter
everyone needs a screenshot once in a while  
supports custom resolution & unity capture screenshot

### Logging
save log messages to timcoded folders outside of app/data folder  
includes message, timecode, stacktrace (optional per logtype)  
support for txt and json  

![](http://uploads.gaisterhand.de/unityLogger.png)