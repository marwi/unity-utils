﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using OpenUtils.Network;

[RequireComponent(typeof(Text))]
public class TitleChanger : MonoBehaviour
{
    public WebserverComponent server;
    public UriParamListener paramListener;

    public string paramName = "title";

    void Start()
    {
        if(server != null && paramListener != null)
        {
            Text.text = "open your browser at " + server.URL + "?" + paramName + "=hallo";
        }
    }

    void OnEnable()
    {
        if (paramListener != null)
        {
            paramListener.Received += OnParamsReceived;
        }
    }

    void OnDisable()
    {
        if (paramListener != null)
        {
            paramListener.Received -= OnParamsReceived;
        }
    }

    private void OnParamsReceived(object sender, UriParamEventArgs e)
    {
        HandleParams(e.uriParams);
    }

    private void HandleParams(Dictionary<string, string> data)
    {
        if (data.ContainsKey(paramName))
        {
            ChangeText(data[paramName]);
        }
    }


    private Text _text;
    private Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponent<Text>();
            return _text;
        }
    }

    private void ChangeText(string newText)
    {
        Text.text = newText;
    }
}
