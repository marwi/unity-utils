﻿using UnityEngine;
using System.Collections;

namespace OpenUtils.Json
{
    /// <summary>
    /// Helper Class to convert json vec to Color. Support for 0-255 and 0-1 format. 
    /// Expecting json to contain r, g, b, a
    /// </summary>
    public class ColorJson
    {
        public float 
            r = 255,
            g = 0,
            b = 255,
            a = 100;

        /// <summary>
        /// Color from 0 to 1
        /// </summary>
        public Color color
        {
            get
            {
                return new Color(r / 255, g / 255, b / 255, a / 255);
            }

        }

        /// <summary>
        /// Color from 0 to 255
        /// </summary>
        public Color color255
        {
            get
            {
                return new Color(r, g, b, a);
            }
        }

        public override string ToString()
        {
            return "[" + r + "," + g + "," + b + "," + a + "]";
        }
    }
}


