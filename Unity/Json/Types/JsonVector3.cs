﻿using UnityEngine;

namespace OpenUtils.Json
{
    /// <summary>
    /// Helper Class to convert json vec structure to Vector3
    /// Expecting json to containt x,y,z
    /// </summary>
    public class Vector3Json
    {
        public float x, y, z;

        public Vector3 vec
        {
            get
            {
                return new Vector3(x, y, z);
            }
        }

        public override string ToString()
        {
            return "[" + x + "," + y + "," + z + "]";
        }
    }
}