﻿using UnityEngine;
using System.Collections;
using OpenUtils.Json;

[RequireComponent(typeof(Camera))]
public class BackgroundColorListener : MonoBehaviour
{
    public MyJsonHandler myJsonFileComponent;

    void OnEnable()
    {
        if (myJsonFileComponent != null)
            myJsonFileComponent.Updated += OnJsonChanged;
    }

    void Start()
    {
        if (myJsonFileComponent != null)
        {
            MyJsonFile file = myJsonFileComponent.result;
            UpdateColor(file);
        }
    }

    private void OnJsonChanged(object sender, FileChangedArgs<MyJsonFile> e)
    {
        MyJsonFile json = e.preset as MyJsonFile;
        if (json != null)
        {
            UpdateColor(json);
        }
    }


    private Camera _cam;
    public Camera cam
    {
        get
        {
            if(_cam == null)
            {
                _cam = GetComponent<Camera>();
            }
            return _cam;
        }
    }

    private void UpdateColor(MyJsonFile json)
    {
        cam.backgroundColor = json.backgroundColor.color;
    }
}
