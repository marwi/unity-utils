﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace OpenUtils.Json
{
    public class FileChangedArgs : EventArgs
    {
        public object preset { get; private set; }

        public FileChangedArgs(object preset)
        {
            this.preset = preset;
        }
    }

    public class FileChangedArgs<T> : FileChangedArgs
    {
        public FileChangedArgs(T preset) : base(preset)
        {
        }
    }

    public abstract class JsonFile : MonoBehaviour
    {
        public event EventHandler<FileChangedArgs> Updated;
        [Tooltip("Relative Path from Asset/Data folder to your .json File")]
        public string relativePath;

        public string absolutePath { get { return Path.Combine(Application.dataPath, relativePath); } }
        public bool exists { get { return File.Exists(absolutePath); } }

        protected object Result;
        public object result
        {
            get
            {
                if (Result == null)
                    Read();

                return Result;
            }

            set
            {
                Result = value;
            }
        }

        #region FileWatching
        FileSystemWatcher watcher = new FileSystemWatcher();
        protected virtual void OnEnable()
        {
            if(exists)
            {
                watcher.Path = Path.GetDirectoryName(absolutePath);
                watcher.Filter = Path.GetFileName(relativePath);
                watcher.NotifyFilter = NotifyFilters.LastWrite;
                watcher.Changed += OnFileChanged;
                watcher.EnableRaisingEvents = true;
            }
        }

        protected virtual void OnDisable()
        {
            watcher.Changed -= OnFileChanged;
            watcher.EnableRaisingEvents = false;
        }

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                // cannot call "Read" directly, because this event happens not on the unity main thread apparently
                fileChanged = true;
            }
        }

        bool fileChanged = false;
        protected virtual void Update()
        {
            if(fileChanged)
            {
                fileChanged = false;
                Read();
            }
        }
        #endregion

        public abstract void Read();
    }


    /// <summary>
    /// handle serialized json files
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [System.Serializable]
    public class JsonFile<T> : JsonFile
    {
        public new event EventHandler<FileChangedArgs<T>> Updated;

        private Reader reader
        {
            get
            {
                return new Reader();
            }
        }

        protected bool errorReadingFile = false;
        protected override void Update()
        {
            base.Update();
            //errorReadingFile = false;
        }

        /// <summary>
        /// Serialized JSON
        /// </summary>
        public new T result
        {
            get
            {
                if (Result == null)
                    Read();
                return (T)Result;
            }

            private set { Result = value; }
        }

        /// <summary>
        /// Re-Read result
        /// </summary>
        public override void Read()
        {
            if(exists && !errorReadingFile)
            {
                result = reader.Read<T>(absolutePath);

                if(result != null)
                {
                    errorReadingFile = false;

                    if (Updated != null)
                        Updated(this, new FileChangedArgs<T>(result));
                }
                else
                {
                    errorReadingFile = true;
                }
            }
            
        }
    }
}

