﻿using UnityEngine;
using System;

namespace OpenUtils.Log
{
    /// <summary>
    /// exposing settings to unity editor
    /// </summary>
    [Serializable]
    public class WriterSettings
    {
        public LogSettings message, warning, error, exception;

        public bool LogStackTrace(LogType type)
        {
            switch (type)
            {
                case LogType.Log: return message.Stacktrace;
                case LogType.Warning: return warning.Stacktrace;
                case LogType.Error: return error.Stacktrace;
                case LogType.Exception: return exception.Stacktrace;
                default: return true;
            }
        }

        public bool DoLog(LogType type)
        {
            switch (type)
            {
                case LogType.Log: return message.log;
                case LogType.Warning: return warning.log;
                case LogType.Error: return error.log;
                case LogType.Exception: return exception.log;
                default: return true;
            }
        }
    }

    [Serializable]
    public struct LogSettings
    {
        public bool log;

        [Tooltip("write stacktrace to file")]
        public bool Stacktrace;
    }

}
