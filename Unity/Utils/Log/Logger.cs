﻿using UnityEngine;
using System.Collections.Generic;

using System;
using System.IO;

namespace OpenUtils.Log
{
    public class Logger : MonoBehaviour
    {
        [Tooltip("name of folder created outside of your Data/App folder")]
        public string folderName = "logs";
        public LogWriterTypes fileType;
        
        public WriterSettings settings;
        
        private string fileExtension
        {
            get
            {
                switch(fileType)
                {
                    case LogWriterTypes.Json:
                        return "json";

                    default:
                    case LogWriterTypes.TextFile:
                        return "txt";
                }
            }
        }
        private string filePath;
        private ILogWriter _writer;
        private ILogWriter Writer
        {
            get
            {
                if (_writer == null)
                {
                    DateTime now = DateTime.Now;

                    string day = now.Year.ToString() + now.Month.ToString() + now.Day.ToString();
                    string directoryPath = Application.dataPath + "/../" + folderName + "/" + day + "/";

                    filePath = directoryPath + DateTime.Now.Timestamp() + "." + fileExtension;
                    if (Directory.Exists(directoryPath) == false)
                        Directory.CreateDirectory(directoryPath);

                    // using fs to be sure not to block the file for first time usage
                    using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite)) { }

                    switch (fileType)
                    {
                        case LogWriterTypes.Json:
                            _writer = new JsonWriter(filePath, settings);
                            break;

                        default:
                        case LogWriterTypes.TextFile:
                            _writer = new TextWriter(filePath, settings);
                            break;
                    }
                }

                return _writer;
            }
        }

        void OnReceiveLog(string message, string stackTrace, LogType type)
        {
            if (settings.DoLog(type) == false)
                return;

            Writer.Write(message, stackTrace, type);
        }

        #region Unity
        void Start()
        {
            //Writer.Write("test", "", LogType.Log);
        }

        void OnEnable()
        {
            Application.logMessageReceived += OnReceiveLog;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= OnReceiveLog;
        }
        #endregion Unity
    }
}
