﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

namespace OpenUtils.Utils
{
    public abstract class AbstractScreenshotter : MonoBehaviour
    {
        public KeyCode screenShotKey = KeyCode.Space;
        public string fileName = "capture";

        private string _directory;
        public string directory
        {
            get { return _directory; }
            set
            {
                if (Directory.Exists(value) == false)
                {
                    Directory.CreateDirectory(value);
                }
                _directory = value;
            }
        }

        public string fullPath
        {
            get
            {
                return Path.Combine(directory, fileName + "-" + DateTime.Now.Timestamp() + ".png");
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(screenShotKey))
            {
                TakeScreenshot();
            }
        }

        public abstract void TakeScreenshot();
    }

}
